#!/bin/bash -x

# if node_modules doesn't exist, run npm install:
if [ ! -d "node_modules" ];
then
  npm install;
else
  echo "node_modules already installed.";
fi
# if [ ! -d "node_modules/cordova" ];
# then
#   npm install cordova;
# else
#   echo "cordova installed already.";
# fi
# if [ ! -d "node_modules/ionic" ];
# then
#   npm install ionic;
# else
#   echo "ionic installed already.";

# fi

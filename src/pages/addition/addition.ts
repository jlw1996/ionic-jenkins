import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import * as assert from 'assert';
  
@Component({
  selector: 'page-addition',
  templateUrl: 'addition.html'
})
export class AdditionPage implements OnInit {
  constructor() {

  }

  public gumballs:number;

  getGumballs() {
      return this.gumballs;
  }

  setGumballs(amount) {
      this.gumballs = amount;
  }

  turnWheel(i) {
      this.setGumballs(this.getGumballs() + i);
  }

  ngOnInit() {
    this.setGumballs(100);

    //Add 1
    // this.turnWheel(1);

    //Remove 1
    this.turnWheel(-1);
    assert.deepStrictEqual(this.getGumballs(), 99)
  }

}
    

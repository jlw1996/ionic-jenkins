#!/bin/bash -x

# this will make it to run every minute and verify if there's something to publish.
# add this entry using: crontab -e
# * * * * * /mnt/efs/ep2admin/jenkins/home/workspace/Dev.EP21.Ionic/jenkins-trigger-publish.sh
# and use to list: crontab -l

JENKINS_BASEPATH=/mnt/efs/ep2admin/jenkins/home/workspace
JENKINS_WSPACE=Dev.EP21.Ionic
JENKINS_WSPACE_PUB=Dev.EP21.Ionic.Publish

CRON_TRIGGER=$JENKINS_BASEPATH/$JENKINS_WSPACE_PUB/jenkins-trigger-publish.cron
CRON_LOG=$JENKINS_BASEPATH/$JENKINS_WSPACE_PUB/jenkins-trigger-publish-latest.log
CRON_LOG_NOACTION=$JENKINS_BASEPATH/$JENKINS_WSPACE_PUB/jenkins-trigger-publish-noaction.log

# if trigger file exists:
if [ -e $CRON_TRIGGER ];
then
  # deletes trigger file:
  rm $CRON_TRIGGER;
  # copies latest Jenkins build to the live www folder served by wrkGateway:
  cp --recursive --update $JENKINS_BASEPATH/$JENKINS_WSPACE/platforms/browser/www/* /mnt/efs/ep2admin/nodejs/modules/ionic/;
  # logs latest date/time it ran:
  echo "published: `date`" > $CRON_LOG;

else
  # logs latest date/time it ran:
  echo "no action: `date`" > $CRON_LOG_NOACTION;
fi
